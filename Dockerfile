FROM alpine

#inspired by https://github.com/gitphill/ldap-alpine.git

ENV ORGANISATION_NAME "Example Inc."
ENV SUFFIX "dc=example,dc=com"
ENV ROOT_USER "admin"
ENV ROOT_PW "admin"
ENV USER_UID "sschneider"
ENV USER_GIVEN_NAME "Schneider"
ENV USER_SURNAME "Stefan"
ENV USER_EMAIL "sschneider@example.com"
ENV ACCESS_CONTROL "access to * by * read"
ENV LOG_LEVEL "stats"

RUN apk add --update openldap openldap-back-mdb openldap-clients nano && \
    mkdir -p /run/openldap /var/lib/openldap/run /var/lib/openldap/openldap-data && \
    rm -rf /var/cache/apk/*

COPY scripts/* /etc/openldap/
COPY docker-entrypoint.sh /

EXPOSE 389
EXPOSE 636

VOLUME ["/ldif", "/var/lib/openldap/openldap-data"]

ENTRYPOINT ["/docker-entrypoint.sh"]
